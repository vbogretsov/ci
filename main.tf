variable "scw_token" {
  type = "string"
}

variable "scw_org" {
  type = "string"
}

variable "scw_region" {
  type   = "string"
  default = "par1"
}

variable "scw_type" {
  type    = "string"
  default = "START1-XS"
}

variable "scw_image" {
  type    = "string"
  default = "67375eb1-f14d-4f02-bb42-6119cecbde51" # Ubuntu_Xenial 25G
}

variable "gitlab-token" {
  type = "string"
}

variable "gitlab-url" {
  type    = "string"
  default = "https://gitlab.com/"
}

provider "scaleway" {
  organization = "${var.scw_org}"
  token        = "${var.scw_token}"
  region       = "${var.scw_region}"
}

resource "scaleway_server" "gitlab-runner" {
  name                = "gitlab-runner"
  image               = "${var.scw_image}"
  type                = "${var.scw_type}"
  dynamic_ip_required = true

  provisioner "file" {
    source      = "config.toml"
    destination = "/opt/config.toml"
  }
  provisioner "file" {
    source      = "Dockerfile"
    destination = "/opt/Dockerfile"
  }

  provisioner "file" {
    source      = "register-runner.sh"
    destination = "/opt/register-runner.sh"
  }

  provisioner "file" {
    source      = "docker-compose.yml"
    destination = "/opt/docker-compose.yml"
  }

  provisioner "file" {
    source      = "install-docker.sh"
    destination = "/opt/install-docker.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /opt/install-docker.sh",
      "chmod +x /opt/register-runner.sh"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "/opt/install-docker.sh"
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "docker-compose -f /opt/docker-compose.yml up -d",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "/opt/register-runner.sh ${var.gitlab-url} ${var.gitlab-token}"
    ]
  }
}
