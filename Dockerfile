FROM alpine:3.8

COPY config.toml /etc/gitlab-runner/

RUN apk update && apk add git && \
    wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 && \
    chmod +x /usr/local/bin/gitlab-runner

WORKDIR /build

# TODO: set user

ENTRYPOINT [ "gitlab-runner" ]
CMD ["run", "--user=gitlab-runner", "--working-directory=/build"]
