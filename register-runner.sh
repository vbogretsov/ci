#! /bin/sh

docker exec gitlab-runner gitlab-runner register \
    --non-interactive \
    --executor "docker" \
    --docker-image alpine:3 \
    --url "$1" \
    --registration-token "$2" \
    --description "docker-runner" \
    --tag-list "docker,scaleway" \
    --run-untagged \
    --locked="false"
